//********************************************
//Janyl Jumadinova
//CMPSC 111 Spring 2015
//Lab 8
//Date: 04/13/2015
//
//Purpose: This class creates an instance of the SudokuChecker object
//         and calls two of its methods.
//********************************************


public class SudokuMain
{
    public static void main (String args [])
    {
        SudokuChecker foo = new SudokuChecker();
        foo.getGrid();
        foo.checkGrid();
    }





}

