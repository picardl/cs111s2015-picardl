//********************************************
//Janyl Jumadinova
//CMPSC 111 Spring 2015
//Lab 8
//Date: 04/13/2015
//
//Purpose:Create a Sudoku checker.
//********************************************

import java.util.Scanner;

public class SudokuChecker
{
    //array
    int count = 0;
    int [] puzzle = new int[16];

    //Constructor: print welcome message and initialize variables
    public SudokuChecker()
    {
        System.out.println("Welcome to Sudoku Checker. This program will validate your 4 by 4 sudoku puzzle.");

        for(int i=0; i < puzzle.length; i++)
        {
            puzzle[i] = 0;
        }
    }

    // get grid method to obtain the input from the user
    public void getGrid()
    {
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter Rows 1 - 4 conseq: ");

        for(int i=0; i < puzzle.length; i++)
        {
            puzzle[i] = scan.nextInt();
        }

    }


    //check if each row, column and region adds up to 10
    // verify that the whole puzzle is correct
    public void checkGrid()
    {
        // check rows
        // TO DO: how to increment i
             for(int i=0; i<13; i+=4)
        {
            if(puzzle[i]+puzzle[i+1]+puzzle[i+2]+puzzle[i+3]==10)
            {
                System.out.println("Row 1 is Correct");
            }

            else if (i==4)
            {
                System.out.println("Row 2 is correct");
            }
            else if (i==8)
            {
                System.out.println("Row 3 is correct");
            }
            else if (i==12)
            {
                System.out.println("Row 4 is correct");
            }
            else
            {
                System.out.println("This Row is not Correct");
            }
        }

        // check columns
        // TO DO: how to increment i
        for(int i=0; i==3; i++)
        {
            if(puzzle[i]+puzzle[i+4]+puzzle[i+4+4]+puzzle[i+4+4+4]==10)
            {
                System.out.println("This Column "+(i+1)+" is Correct");
            }
            else
            {
                System.out.println("This Column "+(i+1)+" is not Correct");
            }
        }

        // check regions
        // TO DO: how to increment i
        for(int i=0; i<12; i++)
        {
            if (i==0 || i==2 || i==8 || i==10)
            if(puzzle[i]+puzzle[i+1]+puzzle[i+4]+puzzle[i+5]==10)
            {
                System.out.println("This Region is Correct");
            }
            else
            {
                System.out.println("This Row is not Correct");
            }
        }

    }

}
