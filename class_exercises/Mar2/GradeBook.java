//=================================================
// Class Example using Math Class
// March 2, 2015
// CMPSC 111, Spring 2015
// Layne Picard
//
// Purpose:  Define class GradeBook with a member method displayMessage
//=================================================


public class GradeBook {
    // method to display a welcome message
    private String CourseName = "music101";
    //constructor
    public GradeBook (String name)
    {
        CourseName = name;
    }
    public void setCourseName (String name)
    {
        CourseName = name;
    }
    public String getCourseName()
    {
       return CourseName;
    }
    public void displayMessage(String name) {
        System.out.println("Welcome to the Grade Book for "+name+"!");
    }

}
