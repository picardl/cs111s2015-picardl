//*********************
// Layne Picard
// Class Exercise
// Februrary 18 2015
//
// Purpose: This program helps the user
// to create a random new identity
//*********************





import java.util.Scanner;
import java.util.Random;

public class NewIdentity
{
	public static void main(String args[])
	{
	      String firstName, lastName, job;
	      int age;
	      float salary;
	      int ss;
	      //create an instance of a scanner
	      Scanner scan = new Scanner(System.in);

	      //create an instance of a random class
	      Random rand = new Random();

	      // get user input
      	      System.out.println("Please enter a first name: ");
	      firstName = scan.nextLine();
	      System.out.println("Please enter a last name: ");
	      lastName = scan.nextLine();
	      System.out.println("Please enter your dream job: ");
	      job = scan.nextLine();
	      System.out.println("Please enter an integer: ");
	      age = scan.nextInt();
	      System.out.println("Please enter a floating point number: ");
	      salary = scan.nextFloat();
	      System.out.println("Please enter your social security number: ");
	      ss = scan.nextInt();
              //
	      //
	      int length = firstName.length();
	      int position = rand.nextInt(length);
	      System.out.println("Random position chosen is "+position);
	      //pick a character at that position
	      char randomLetter = firstName.charAt(position);
	      System.out.println("Random letter at "+position+" is "+randomLetter);
	      //replace all of selected characters with 'a'
	      char randomLetter2 = firstName.charAt(3);
	      
	      firstName = firstName.replace(randomLetter, randomLetter2);
	      //append 'ov' to the lastName string
	      lastName = lastName.concat("ov");

	       //change the job to upper case
	       job = job.toUpperCase();

	       // assign a value for the age
	       age = rand.nextInt(age);

	       // assign a value for salary
	       salary = rand.nextFloat()*salary+1000;

	       System.out.println("Your new name is : "+firstName+" "+lastName+", and you are "+age+" years old.\n You work as a "+job+" making "+salary+" a year. your new ss number is "+ss);
	}
}
