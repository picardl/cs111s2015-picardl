//=================================================
// Class Example using Graphics
// February 11, 2015
// Layne Picard
// Original submission by Allison Cabo in CMPSC 111, Fall 2014
// Purpose: To create a unique drawing.
//=================================================
import javax.swing.*;
import java.awt.*;

public class Drawing extends JApplet  {
	public void paint(Graphics page)
	 {
		final int WIDTH = 600;
		final int HEIGHT = 400;

		page.setColor(Color.cyan);
		page.fillRect(0, 0, WIDTH, HEIGHT);

		page.setColor(Color.black);
		page.fillRect(325, 250, 80, 80);
		page.fillRect(325, 200, 5, 50);
		page.fillRect(350, 200, 5, 50);
		page.fillRect(375, 200, 5, 50);
		page.fillRect(400, 200, 5, 50);


		page.setColor(Color.red);
		page.fillOval(260, 30, 200, 200);

		
		page.setColor(Color.blue);
		page.fillOval(210, 250, 90, 90);
		page.fillRect(235, 305, 50, 50);
		page.fillOval(226, 340, 100, 70);

		page.setColor(Color.white);
		page.fillOval(30, 170, 90, 90);
		page.fillOval(55, 130, 90, 90);
		page.fillOval(80, 160, 95, 90);

		page.fillOval(230, 270, 10, 10);
		page.fillOval(260, 270, 10, 10);

		page.setColor(Color.black);
		page.fillOval(90, 200, 50, 10);
		page.fillOval(125, 200, 50, 10);
		page.fillOval(300, 50, 50, 10);
		page.fillOval(260, 50, 50, 10);
		page.fillOval(232, 272, 5, 5);
		page.fillOval(262, 272, 5, 5);





		page.setColor(Color.yellow);
		page.fillOval(0, 0, 50, 50);
		page.fillArc(200, 285, 80, 50, 80, 90);


		
	}
	
	// main method
	public static void main(String[] args)
    	{
        	JFrame window = new JFrame("Janyl Jumadinova ");

      		// Add the drawing canvas and do necessary things to
     		// make the window appear on the screen!
        	window.getContentPane().add(new Drawing());
        	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        	window.setVisible(true);
			window.setSize(600, 400);
        	
        	//window.pack();
    	}

} // end class 
