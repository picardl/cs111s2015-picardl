/*
Class exercise March 09, 2015
Layne Picard
Purpose: create with the main method that creates instances
of the Account objects and uses its methods
*/

import java.util.Scanner;

public class AccountMain
{
    public static void main (String args[])
    {
        //create instances of account object
        Account smith = new Account(50.00);
        Account jones = new Account(-5.12);

        //call get balance method and print the value that is returned
        System.out.println("Balance for smith is " +smith.getBalance());
        System.out.println("Balance for jones is " +jones.getBalance());

        //call withdraw and deposit methods
        smith.withdraw(10.92);
        jones.deposit(1000);

        //call get balance method and print the value that is returned
        System.out.println("New balance for smith is " +smith.getBalance());
        System.out.println("New balance for jones is " +jones.getBalance());

        Scanner input = new Scanner(System.in);
        System.out.println("Smith, enter a value to deposit");
        double value = input.nextDouble();
        smith.deposit(value);

        //call get balance method and print the value that is returned
        System.out.println("New balance for smith is " +smith.getBalance());

        //another instance using constructor 2
        Account smith1 = new Account(10, 40.24);
        System.out.println("Balance for smith 1 is "+smith1.getBalance());
    }

}



