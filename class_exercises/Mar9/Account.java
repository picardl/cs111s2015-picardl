/*
Class exercise March 09, 2015
Layne Picard
Purpose: create an account object with a constructor and 3 methods
*/

public class Account
{
    //instance variable
    private double balance;
    private int acNumber;

    //constructor
    public Account (double initBalance)
    {
        balance = initBalance;
    }

    public Account (int accountNo, double initBalance)
    {
        acNumber = accountNo;
        balance = initBalance;
    }

    //deposit method: adds amount to the balance
    public void deposit (double amount)
    {
        balance += amount;
    }

    //withdraw method: subtracts amount from the balance
    public void withdraw (double amount)
    {
        balance -= amount;
    }
    //getBalance method: returns the value of the balance
    public double getBalance()
    {
        return balance;
    }

}
