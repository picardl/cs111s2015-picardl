//*************************************
// Layne Picard
// CMPSC 111 Spring 2015
// Date: January 26, 2015
//
// Purpose: Strings
// ***********************************
public class StringExample
{
	public static void main ( String args[] )
	{
		System.out.println("Adding "+ 12 + 23);
		System.out.println("Adding "+ (12 + 23));
		System.out.println("I am quite tired today");
		System.out.println("I have " + (1+1) + (" arms.\n but I also havetwo legs."));
	}
}

