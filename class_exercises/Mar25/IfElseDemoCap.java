/* CMPSC 111 Spring 2015
  Layne Picard
  Class Example
  March 23, 2015

  Purpose: To determine whether a grade a vowel (lower-case or upper-case)

*/

import java.util.Scanner;
public class IfElseDemoCap
{
 	public static void main ( String args[] )
 	{
		Scanner input = new Scanner ( System.in );
 		System.out.print ( "Enter a character to test: " );
 	 	char character;
 		character = input.next().charAt(0);	      // get character from input
 	 	if ((character == 'a') || (character == 'A'))     // notice ' ' marks char
 		       	System.out.println ( character+" is a vowel." );
 	 	else if ((character == 'e') || (character == 'E'))
 			System.out.println ( character+" is a vowel." );
 	 	else if ((character == 'i') || (character == 'I'))
 			System.out.println ( character+" is a vowel." );
  		else if ((character == 'o') || (character == 'O'))
 			System.out.println ( character+" is a vowel." );
 		else if ((character == 'u') || (character == 'U'))
 			System.out.println ( character+" is a vowel." );
 		else
 			System.out.println ( character+" is not a vowel." );
 	}
}

