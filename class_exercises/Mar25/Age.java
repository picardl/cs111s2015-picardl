

//=================================================
// Class Example using if/else statements
// March 25, 2015
// CMPSC 111, Spring 2015
// Layne Picard
//
// Purpose: Age class has a constructor and two methods:
// updateAge to change the value of age to user specified value
// checkAge to check the age of the user
//=================================================/*
public class Age
{
    // instance variable
    private int age;

    // constructor
    public Age ( )
    {
        // initialize
        age = 0;
            }
    public void updateAge(int a)
    {
        age = a;
    }
    //check if age is appropriate
    public boolean checkAge()
    {
        if (age < 12)
        {
            return false;
        }
        else if ((age >= 12) && (age < 18))
        {
            System.out.println("Please get parent's consent");
            return true;
        }
        else
        {
            return true;
        }
        }
}
