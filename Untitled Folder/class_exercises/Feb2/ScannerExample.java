/***********************
 * Janyl Jumadinova
 * Class Exercise
 * February 2, 2015
 * *********************/

import java.util.Scanner;

public class ScannerExample
{
	public static void main (String[] args)
	{
		Scanner input = new Scanner (System.in);

		// variables
		String food, hobby;
		int num;
		int sum;

		System.out.print("Enter your favourite food: ");
		food = input.nextLine();

		System.out.print("Enter your hobby: ");
		hobby = input.nextLine();
		
		System.out.println("You entered "+food+" and "+hobby);

		System.out.print("Enter a number: ");
		num = input.nextInt();

		System.out.println("num "+num+", pre increment "+(++num)+", post increment "+(num++));

		sum = num++;
		System.out.println("Sum is "+sum);

		sum = ++num;
		System.out.println("Sum is "+sum);
	}
}
