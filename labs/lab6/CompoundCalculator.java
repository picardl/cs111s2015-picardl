//=================================================
// The work I am submitting is a result of my own thinking and efforts
//Class Example using Math Class and formatting
// February 25, 2015
// CMPSC 111, Spring 2015
// Layne Picard
//
// Purpose: Write a program that calculates compound
// interest using math and format classes
//=================================================
import java.util.Scanner;
import java.text.NumberFormat;
import java.util.Date;
public class CompoundCalculator
{
  	public static void main ( String args[] )
    {

        double p, a, r, A;
        int t, n;
        //Label output with name and date:
	    System.out.println("Layne Picard\nLab 6\n" + new Date() + "\n");
        //declaring scanner
        Scanner scan = new Scanner(System.in);
        //applying formats for decimal, currency and percent
        NumberFormat fmt1 = NumberFormat.getCurrencyInstance();
        NumberFormat fmt2 = NumberFormat.getPercentInstance();
        //asking user to enter amount of money borrowed
        System.out.print("Enter the amount of money you have borrowed: ");
        p = scan.nextDouble();
        //showing what the user entered in dollar form
        System.out.println("You entered "+fmt1.format(p)+"\n");
        //asking user to enter number of years the money is borrowed for and displaying value
        System.out.print("Enter the number of years you borrowed if for: ");
        t = scan.nextInt();
        System.out.println("You entered "+t+" years\n");

        //asking user to enter amount of times compounded per year and displaying that value
        System.out.print("Enter the number of times the interest is compounded per year: ");
        n = scan.nextInt();
        System.out.println("You entered "+n+" times per year\n");
        //choosing an interest rate and displaying that to the user in percent form
        r = Math.random()*.13+.02;
        System.out.println("Interest chosen is "+fmt2.format(r)+"\n");

        //calculating amount of money needed to pay
        a = p*Math.pow(1+(r/n), n*t);
        //displaying the amount of money needed to pay at for t years at r interest compounded n times per year
        System.out.println("Borrowing "+fmt1.format(p)+" for "+t+" years at "+fmt2.format(r)+" interest, compounded "+n+" times per year:\nThe amount you have to pay is "+fmt1.format(a)+"\n");
        //calculating amount of money needed to pay if it is compounded continuously and displaying that to the user
        A = p*Math.exp(r*t);
        System.out.println("Borrowing the same amount at "+fmt2.format(r)+" compounded continuously, \nyou have to pay "+fmt1.format(A));
    }
}
