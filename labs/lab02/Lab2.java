
//*************************************
// Layne Picard
// 29 January 2015
// Lab02
// The work I am submitting is a result of my own thinking and efforts
// Purpose: to compute and print the number of yards between the Earth and the moon, then print lunar maximum and minimum temperatures in both celsius and fahrenheit. */

import java.util.Date;

public class Lab2
{

    //------------------------
    // main method: program execution begins here
    //------------------------
    public static void main(String[] args)
    {
	//Label output with name and date:
	System.out.println("Layne Picard\nLab 2\n" + new Date() + "\n");

	//Variables:
	int milesToMoon = 238900;    //distance to moon in miles
	int ydsPerMile = 1760;       //number of yards in a mile
	int ydsToMoon; //number of yards to the moon
	//compute values:
	ydsToMoon = milesToMoon * ydsPerMile;

	System.out.println("Distance to the moon in miles: " + milesToMoon);
	System.out.println("The numer of yards per mile: " + ydsPerMile);
	System.out.println("The number of yards from the earth:");
	System.out.println("to the moon is " + ydsToMoon);

	int ftToMoon;
	int ftPerYard = 3;
	ftToMoon = ydsToMoon * ftPerYard;

	System.out.println("The number of feet to the moon is " + ftToMoon);
	double armsToMoon;
	double ftPerArm = 2.083;
	armsToMoon = ftToMoon / ftPerArm;
	System.out.println("The average arm is " + ftPerArm + (" feet."));
	System.out.println("The number of arms to the moon is " + armsToMoon);
	
	double twoArmsMoon;
	double twoArms = 2.083 + 2.083;
	twoArmsMoon = ftToMoon / twoArms;
	System.out.println("The number of pairs of arms to the moon is " + twoArmsMoon); 
	
    }
}
