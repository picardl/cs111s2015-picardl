
//*************************************
// Layne Picard
// 05 February 2015
// Lab03
// The work I am submitting is a result of my own thinking and efforts
// Purpose: To write a program that calculates the tip, total bill and the amount per person each should pay.
// To gain more experience with variables/expressionsand using user inputs/applying scanner object.
import java.util.Date;
import java.util.Scanner;

public class TipCalculator
{

    //------------------------
    // main method: program execution begins here
    //------------------------
    public static void main(String[] args)
    {
	//Label output with name and date:
	System.out.println("Layne Picard\nLab 3\n" + new Date() + "\n");
	//Declaring Scanner
	Scanner input = new Scanner (System.in);
	//Declaring variables
	String name;
	double bill;
	double tipdesired;
	//asking user to enter name
	System.out.print("Enter your name: ");
	name = input.nextLine();
	//Saying hello using the name entered by the user and asking them their bill amount
	System.out.println("Hi "+name+"! Welcome to Tip Calculator!\nI hope your day is going well!\n Enter your bill amount: ");
	bill = input.nextDouble();

	//Asking user to enter tip amount
	System.out.print("Enter the desired tip percetage as a decimal number between 0 and 1: ");
	tipdesired = input.nextDouble();

	//declaring variables
	double tip = bill*tipdesired;
	double total = tip+bill;
	
	//Telling the user what the total tip and total bill. 
	System.out.println("So, "+name+" the original bill before the tip was : $"+bill+".\nThe tip amount is $"+tip+".\nThe total bill is $"+total+".");

	//Declaring people and Also asking how many people are splitting the bill
	int people;
	System.out.print("How many people will be splitting the bill? ");
	people = input.nextInt();

	//Declaring a variable that is the amount each person should pay, telling the user what that amount is and saying goodbye
	double splitbill = total/people;
	System.out.println("Each person should pay $"+splitbill+".\nThank you for using our service! Have a nice day and I hope you enjoyed your meal!!");
	

    }
}
