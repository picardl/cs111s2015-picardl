//*************************************
// Honor Code: The work I am submitting is
// a result of my own thinking and efforts.
// Layne Picard
// CMPSC 111 Spring 2015
// Lab 1 Part 2
// Date: 01 22 2015
//
// Purpose: Learning how to write a text file and troubleshoot any errors.
// ************************************

import java.util.Date;

public class Lab1
{
    public static void main ( String args[] )
    {
	System.out.println("Layne Picard"+ new Date());
	System.out.println("Lab 1");

	//Prints words of wisdom

	System.out.println("Advice from Jajes Gosling, creator of Java:");
	System.out.println("Don't be intimidated--give it a try!");
    }
}
