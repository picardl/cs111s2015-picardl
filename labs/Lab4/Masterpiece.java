//=================================================
// Honor Code: The work I am submitting is 
// a result of my own thinking and efforts.

// Layne Picard
// CMPSC 111 Spring 2015
// Lab 4
// Date: 12 February 2015
//
// Purpose: My program is a drawing of a bag of skittles with the caption "Taste the Rainbow" at the bottom
//=================================================
import java.awt.*;
import javax.swing.*;

public class Masterpiece extends JApplet
{
    //-------------------------------------------------
    // Use Graphics methods to add content to the drawing canvas
    //-------------------------------------------------
    public void paint(Graphics page)
    {     	
		int x = 5; //setting x coordinate
		int y = 5; //setting y coordinate
		int width = 50; //setting width
		int height = 50; //setting height

		page.setColor(Color.lightGray); //change color to light gray
		page.fillRect(0,0,width*12,height*8);// making background

		
		page.setColor(Color.red); //change color to red
		page.fillRect(x*20,y*20,width*7,height*3); //making Skittles bag
		page.setColor(Color.red); //set color to red ?

		page.drawString("S" , x*31, y*48); //Drawing String... Not sure
						  //why but this is the only
						  // way I got the other text
						  // to be white	
		page.setColor(Color.yellow); //change color to yellow
		page.fillOval(x*29, y*37, width/3, height/3); //drawing yellow skittle

		page.setColor(Color.orange); //change color to orange
		page.fillOval(x*40, y*36, width/3, height/3); //drawing orange skittle
		page.setColor(Color.magenta); //change color to magenta
		page.fillOval(x*60, y*42, width/3, height/3); //drawing magenta skittle
		page.setColor(Color.green); //change color to green
		page.fillOval(x*55, y*32, width/3, height/3); //drawing green skittle
	
 		page.setFont(new Font("Skits", Font.BOLD, 55)); //make font bold and larger


		page.setColor(Color.white); //change color to white

		page.drawString("SKITTLES" , x*26, y*32); //make string Skittles
		page.setFont(new Font("S", Font.BOLD, 9)); //changing font size for the "s" on the skittles
		page.drawString("S" , x*30, y*40); //make S on skittles for each skittle
		page.drawString("S" , x*41, y*38);
		page.drawString("S" , x*61, y*44);
		page.drawString("S" , x*56, y*34);
		page.setFont(new Font("Rainbow", Font.BOLD, 40)); //changing font size for "taste the rainbow"
		page.drawString("TASTE THE RAINBOW", x*15, y*60); //make string "taste the rainbow"


	

    }

    // main method that runs the program
    public static void main(String[] args)
    {
        	JFrame window = new JFrame(" Layne Picard ");

      		// Add the drawing canvas and do necessary things to
     		// make the window appear on the screen!
        	window.getContentPane().add(new Masterpiece());
        	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        	window.setVisible(true);
		window.setSize(600, 400);
        }
}
