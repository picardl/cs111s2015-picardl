//=================================================
// Honor Code: The work I am submitting is
// a result of my own thinking and efforts.

// Layne Picard
// CMPSC 111 Spring 2015
// Lab 7
// Date: 03/7/15
//
// Purpose: This Program is a replicate of my original masterpiece
// but instead uses a masterpiece class that imports
//=================================================
import java.awt.*;
import javax.swing.*;

public class MasterpieceDrawing extends JApplet
{
    //-------------------------------------------------
    // Use Graphics methods to add content to the drawing canvas
    //-------------------------------------------------
    public void paint(Graphics page)
    {
    Masterpiece Mst;
    Masterpiece Mst1;
    Masterpiece Mst2;
    Masterpiece Mst3;
    Masterpiece Mst4;
	// create an instance of Masterpiece class

	// call the methods in the Masterpiece class
	// NOTE: you may have to pass 'page' in addition to any
	//       other arguments during your method call
    page.setColor(Color.lightGray);
    Mst = new Masterpiece(0, 0, 600, 400, "nothing");
    page.fillRect(Mst.getxvalue(), Mst.getyvalue(), Mst.getwidth(), Mst.getheight());

    page.setColor(Color.red);
    Mst1 = new Masterpiece(100, 100, 350, 150, "S");
    page.fillRect(Mst1.getxvalue(), Mst1.getyvalue(), Mst1.getwidth(), Mst1.getheight());

    	page.setColor(Color.red); //set color to red ?
		page.drawString(Mst1.getskittles() , 5*31, 5*48); //Drawing String... Not sure
		//why but this is the only
						  // way I got the other text
						  // to be white

    page.setColor(Color.yellow);
    Mst2 = new Masterpiece(5*29, 5*37, 50/3, 50/3, "SKITTLES");
    page.fillOval(Mst2.getxvalue(), Mst2.getyvalue(), Mst2.getwidth(), Mst2.getheight());
    page.setColor(Color.orange);
    page.fillOval(Mst2.getxvalue()+55, Mst2.getyvalue()-5, Mst2.getwidth(), Mst2.getheight());
    page.setColor(Color.magenta);
    page.fillOval(Mst2.getxvalue()+(5*31), Mst2.getyvalue()+25, Mst2.getwidth(), Mst2.getheight());
    page.setColor(Color.green);
    page.fillOval(Mst2.getxvalue()+(5*26), Mst2.getyvalue()-25, Mst2.getwidth(), Mst2.getheight());

    page.setFont(new Font("Skits", Font.BOLD, 55)); //make font bold and larger
	page.setColor(Color.white); //change color to white

	page.drawString(Mst2.getskittles(), Mst2.getxvalue()-15, Mst2.getyvalue()-25); //make string Skittles

    Mst3 = new Masterpiece(150, 200, 0, 0, "S");
    page.setFont(new Font("S", Font.BOLD, 9));

    page.drawString(Mst3.getskittles(), Mst3.getxvalue(), Mst3.getyvalue());
    page.drawString(Mst3.getskittles(), Mst3.getxvalue()+55, Mst3.getyvalue()-10);
    page.drawString(Mst3.getskittles(), Mst3.getxvalue()+155, Mst3.getyvalue()+20);
    page.drawString(Mst3.getskittles(), Mst3.getxvalue()+130, Mst3.getyvalue()-30);

    Mst4 = new Masterpiece(75, 300, 0, 0, "TASTE THE RAINBOW");
    page.setFont(new Font("Rainbow", Font.BOLD, 40));
    page.drawString(Mst4.getskittles(), Mst4.getxvalue(), Mst4.getyvalue());
    }

    // main method that runs the program
    public static void main(String[] args)
    {
        	JFrame window = new JFrame(" Layne Picard ");

      		// Add the drawing canvas and do necessary things to
     		// make the window appear on the screen!
        	window.getContentPane().add(new MasterpieceDrawing());
        	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        	window.setVisible(true);
		window.setSize(600, 400);

    }
}
