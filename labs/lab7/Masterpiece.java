//=================================================
// Honor Code: The work I am submitting is
// a result of my own thinking and efforts.

// Layne Picard
// CMPSC 111 Spring 2015
// Lab 4
// Date: 7 March 2015
//
// Purpose: My program is a drawing of a bag of skittles with the caption "Taste the Rainbow" at the bottom
//=================================================
import java.awt.*;
import javax.swing.*;

public class Masterpiece extends JApplet
{

    //-------------------------------------------------
    // Use Graphics methods to add content to the drawing canvas
    //-------------------------------------------------


    public	int xvalue; // x = 5; //setting x coordinate
	public int yvalue; //y = 5; //setting y coordinate
	public int width; //= 50; //setting width
	public int height; //= 50; //setting height
    public String skittles;

    public Masterpiece(int x, int y, int w, int h, String s)
    {
        skittles = s;
        xvalue = x;
        yvalue = y;
        width = w;
        height = h;
    }


    public void setxvalue(int x)
    {
        xvalue = x;
    }

    public int getxvalue()
    {
        return xvalue;
    }

    public void setyvalue (int y)
    {
        yvalue = y;
    }
    public int getyvalue()
    {
        return yvalue;
    }
    public void setwidth(int w)
    {
        width = w;
    }
    public int getwidth()
    {
        return width;
    }
    public void setheight(int h)
    {
        height = h;
    }
    public int getheight()
    {
        return height;
    }
    public void setskittles(String s)
    {
        skittles = s;
    }
    public String getskittles()
    {
        return skittles;
    }

    }


