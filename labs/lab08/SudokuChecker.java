//=================================================
// Lab 8
// // March 26, 2015
// CMPSC 111, Spring 2015
// Layne Picard
//
// Purpose: This class gets an age input from the user and
// then it connects to the class called 'Age' to check the age
//=================================================

import java.util.Scanner;

public class SudokuChecker
{
    private int w1, w2, w3, w4;
    private int x1, x2, x3, x4;
    private int y1, y2, y3, y4;
    private int z1, z2, z3, z4;

    public SudokuChecker()
    {
    System.out.println("Welcome to the Sudoku Checker!\n");
    System.out.println("This program checks simple, small 4x4 Sudoku grids for correctness");
    System.out.println("Each column, row and 2x2 region contains the numbers 1 through 4 only once\n");
    System.out.println("To check your Sudoku, enter your board one row at a time\n with each digit separated by a space.  Hit ENTER at the end of a row");
    }
    Scanner s = new Scanner (System.in);
    public void getGrid()
    {
        System.out.print("Please enter the first row: ");
        w1 = s.nextInt();
        w2 = s.nextInt();
        w3 = s.nextInt();
        w4 = s.nextInt();
        System.out.print("Please enter the second row: ");
        x1 = s.nextInt();
        x2 = s.nextInt();
        x3 = s.nextInt();
        x4 = s.nextInt();
        System.out.print("Please enter the third row: ");
        y1 = s.nextInt();
        y2 = s.nextInt();
        y3 = s.nextInt();
        y4 = s.nextInt();
        System.out.print("Please enter the last row: ");
        z1 = s.nextInt();
        z2 = s.nextInt();
        z3 = s.nextInt();
        z4 = s.nextInt();
    }
    public void checkGrid()
    {
        boolean check = true;
        int row1 = w1+w2+w3+w4;
        if (row1==10)
        {
            System.out.println("Row 1: Good");
        }
        else
        {
            System.out.println("Row 1: No Good");
            check = false;
        }
        int row2 = x1+x2+x3+x4;
        if (row2==10)
        {
            System.out.println("Row 2: Good");
        }
        else
        {
            System.out.println("Row 2: No Good");
            check = false;
        }
        int row3 = y1+y2+y3+y4;
        if (row3==10)
        {
            System.out.println("Row 3: Good");
        }
        else
        {
            System.out.println("Row 3: No Good");
            check = false;
        }
        int row4 = z1+z2+z3+z4;
        if (row4==10)
        {
            System.out.println("Row 4: Good");
        }
        else
        {
            System.out.println("Row 4: No Good");
            check = false;
        }
        int reg1 = w1+w2+x1+x2;
        if (reg1==10)
        {
            System.out.println("Reg 1: Good");
        }
        else
        {
            System.out.println("Reg 1: No Good");
            check = false;
        }
        int reg2 = w3+w4+x3+x4;
        if (reg2==10)
        {
            System.out.println("Reg 2: Good");
        }
        else
        {
            System.out.println("Reg 2: No Good");
            check = false;
        }
        int reg3 = y1+y2+z1+z2;
        if (reg3==10)
        {
            System.out.println("Reg 3: Good");
        }
        else
        {
            System.out.println("Reg 3: No Good");
            check = false;
        }
        int reg4 = y3+y4+z3+z4;
        if (reg4==10)
        {
            System.out.println("Reg 4: Good");
        }
        else
        {
            System.out.println("Reg 4: No Good");
            check = false;
        }
        int col1 = w1+x1+y1+z1;
        if (col1==10)
        {
            System.out.println("Col 1: Good");
        }
        else
        {
            System.out.println("Col 1: No Good");
            check = false;
        }
        int col2 = w2+x2+y2+z2;
        if (col2==10)
        {
            System.out.println("Col 2: Good");
        }
        else
        {
            System.out.println("Col 2: No Good");
            check = false;
        }
         int col3 = w3+x3+y3+z3;
        if (col3==10)
        {
            System.out.println("Col 3: Good");
        }
        else
        {
            System.out.println("Col 3: No Good");
            check = false;
        }
        int col4 = w4+x4+y4+z4;
        if (col4==10)
        {
            System.out.println("Col 4: Good");
        }
        else
        {
            System.out.println("Col 4: No Good");
            check = false;
        }
        if (check==true)
        {
            System.out.println("SUDO: Nice");
        }
        else
        {
            System.out.println("SUDO: Not Nice");
        }

    }



}
