//=================================================
// Lab 8 main
// March 26, 2015
// CMPSC 111, Spring 2015
// Layne Picard
//
// Purpose: This class gets an age input from the user and
// then it connects to the class called 'Age' to check the age
//=================================================
import java.util.Date;

public class SudokuMain
{
    public static void main(String args[] )
    {
        System.out.println("Layne Picard\nLab 8\n" + new Date() + "\n");
        SudokuChecker foo = new SudokuChecker();
        foo.getGrid();
        foo.checkGrid();
    }
}
