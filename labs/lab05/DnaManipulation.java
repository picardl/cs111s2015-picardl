//*************************************
//The work I am submitting is a result of my own thinking and efforts
//Layne Picard
// CMPSC 111 Spring 2015
// Lab #5
// Date: 19 February 2015
//
// The purpose of this program is to illustrate several common methods
// of the String class
//*************************************

import java.util.Scanner;
import java.util.Random;

public class DnaManipulation
{
    public static void main(String[] args)
    {
       //Label output with name and date:
	    System.out.println("Layne Picard\nLab 5\n" + new Date() + "\n");
        Scanner scan = new Scanner(System.in); //setting scanner
        Random r = new Random(); //setting random generator
        int length; //declaring variables
        int location;
        char c;
        String dnaString, rnaString, rnaMutation1, rnaMutation2, rnaMutation3;
        //asking user to enter a dna strand
        System.out.println("Enter string containing only C, G, T and A: ");
        dnaString = scan.next(); //reading user input
        dnaString = dnaString.toLowerCase(); //changing everything to lowercase
        //creating complementary rna strand
        rnaString = dnaString.replace('a', 'T');
        rnaString = rnaString.replace('t', 'A');
        rnaString = rnaString.replace('g', 'C');
        rnaString = rnaString.replace('c', 'G');

        //telling user what the complement is
        System.out.println("The complement of "+dnaString+" is: "+rnaString);
        length = rnaString.length();//reading length of complement
        location = r.nextInt(length+1); //choosing a random location
         c = "GTAC".charAt(r.nextInt(3)); //choosing random character
         rnaMutation1 = rnaString.substring(0,location)+c+rnaString.substring(location);//inserting random character
        System.out.println("Inserting a "+c+" at location "+location+" gives "+rnaMutation1);//displaying the mutation to the user
        //reading length of complement again
        length = rnaString.length();
        location = r.nextInt(length);//choosing random location
        //deleting character out of rna strand
        rnaMutation2 = rnaString.substring(0,location) + rnaString.substring(location+1);
        //showing the user the deletion mutation
        System.out.println("Deleting from position "+location+" gives "+rnaMutation2);

        //reading length of complement again
        length = rnaString.length();
        location = r.nextInt(length); //choosing random location
        c = "GTAC".charAt(r.nextInt(3));//choosing random character
        //changing character in rna strand
        rnaMutation3 = rnaString.substring(0,location) + c + rnaString.substring(location+1);
        //showing the user the mutation
        System.out.println("Changing position "+location+" gives "+rnaMutation3);


    }
}
