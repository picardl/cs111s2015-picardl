//*****************************
// Layne Picard
//CMPSC 111
// Practical 7
// March 26, 2015
//
// Purpose: a program that determines what activities happen
// during a specific year.
//*****************************

import java.util.Date;
import java.util.Scanner;
public class Practical7Main
{
    public static void main ( String[] args)
    {
        //Variable dictionary
        Scanner scan = new Scanner(System.in);
        int userInput;

        System.out.println("Please enter a year between 1000 and 3000!");
        userInput = scan.nextInt();

        YearChecker activities = new YearChecker(userInput);

        // TO DO: include method calls

        // call leapyear
        boolean leap = activities.isLeapYear();
        if (leap!=true)
        {
            System.out.println(userInput+" is not a leap year");
        }
        else
        {
            System.out.println(userInput+" is a leap year");
        }
            // call cicada
        boolean cicada = activities.isCicadaYear();
        if (cicada != true)
        {
            System.out.println(userInput+" is not year of emergence for Brood II of the 17-year cicadas");

        }
        else{
            System.out.println(userInput+" is a year of emergence for Brood II of the 17-year cicadas");

        }

        // call sunspot
        boolean sun = activities.isSunspotYear();
        if (sun != true)
        {
            System.out.println(userInput+" is not a peak sunspot year");

        }
        else{
            System.out.println(userInput+" is a peak sunspot year");
        }

        System.out.println("Thank you for using this program.");
    }
}

