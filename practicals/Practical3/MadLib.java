//*************************************
// Layne Picard
// 05 February 2015
// Practical 3
// The work I am submitting is a result of my own thinking and efforts
// Purpose: Using User inputs to print a story using those inputs.
import java.util.Date;
import java.util.Scanner;

public class MadLib
{

    //------------------------
    // main method: program execution begins here
    //------------------------
    public static void main(String[] args)
    {
	//Label output with name and date:
	System.out.println("Layne Picard\nLab #\n" + new Date() + "\n");

	Scanner input = new Scanner (System.in);
	//Variable dictionary:
	String candy, size, verb;
	double num1;
	double num2;
	double num3;
	double num4;

	System.out.print("Enter your favorite candy (plural form): ");
	candy = input.nextLine();
    if ((candy == "skittles") || (candy == "Skittles"))
        System.out.println("You have good taste");
    else
        System.out.println("I think you meant skittles");

	System.out.print("Enter an adjective that describes size: ");
	size = input.nextLine();

	System.out.print("Enter a singular verb: ");
	verb = input.nextLine();

	System.out.print("Enter a number from 1-50: ");
	num1 = input.nextDouble();

	System.out.print("Enter a number from 1-500: ");
	num2 = input.nextDouble();

	System.out.print("Enter a number from 10-20: ");
	num3 = input.nextDouble();

	System.out.print("Enter a number from 1-20: ");
	num4 = input.nextDouble();


	double num5 = (num2/num3)*num4;



	System.out.println("When you eat skittles your arms can change in size. You originally have "+size+" arms, meaning they are "+num1+" inches in circumference, but when you eat "+num2+" skittles your arms grow by "+num3+" inches. How many skittles have you eaten if your arms have grown "+num4+" inches?\nAnswer: You need to eat "+num5+" skittles.\nWhen in doubt, always "+verb+" your arms.");


    }
}
