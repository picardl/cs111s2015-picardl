//*************************************
// Layne Picard
// Practical 2
// 29 January 2015
// Print Something Interesting
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Purpose: To make an interesting text document using various functions, such as println and \n
//*************************************
import java.util.Date;
public class PrintName
{
    public static void main(String[] args)
    {
	System.out.println("Layne Picard. CMPSC 11\n" + new Date() + "\n");
	System.out.println("        _   _   _");
	System.out.println("    _  | | | | | |  ");
	System.out.println("   | | | | | | | |  _");
	System.out.println("   | | | | | | | | | | \n    \\  V  V  V  V| | |\n     \\            V  /");
   	System.out.println("      |             |");
   	System.out.println("       \\            /\n        \\__      __/\n           |    |\n           |    |\n           |    |\n           |    |\n           |    |\n           |    |");
    } 
} 	
