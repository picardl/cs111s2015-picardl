//====================================
// Layne Picard
// The work I am submitting is a result of my own thinking and efforts
// CMPSC 111
// Practical 6
// 5--6 March 2015
//
// This program describes an octopus in the kitchen.
//====================================

import java.util.Date;

public class Practical6
{
    public static void main(String[] args)
    {
        System.out.println("Janyl Jumadinova\n" + new Date() + "\n");

        // Variable dictionary:
        Octopus ocky;
        Octopus ocky1; // an octopus
        Utensil spat;           // a kitchen utensil
        Utensil spat1;

        spat = new Utensil("spatula"); // create a spatula
        spat.setColor("rainbow");        // set spatula properties--color...
        spat.setCost(1000000);           // ... and price

        ocky = new Octopus("Arms", 22);    // create and name the octopus
        //ocky.setAge(10);               // set the octopus's age...
        ocky.setWeight(485);           // ... weight,...
        ocky.setUtensil(spat);         // ... and favorite utensil.

        System.out.println("Testing 'get' methods:");
        System.out.println(ocky.getName() + " weighs " +ocky.getWeight()
            + " pounds\n" + "and is " + ocky.getAge()
            + " years old. His favorite utensil is a "
            + ocky.getUtensil());

        System.out.println(ocky.getName() + "'s " + ocky.getUtensil() + " costs $"
            + ocky.getUtensil().getCost());
        System.out.println("Utensil's color: " + spat.getColor());

        // Use methods to change some values:
        spat1 = new Utensil("skittles crusher");
        spat1.setColor("red");
        spat1.setCost(59.68);

        ocky1 = new Octopus("Arms", 23);


        ocky1.setWeight(400);
        ocky1.setUtensil(spat1);


        //ocky.setAge(20);

        System.out.println("\nTesting 'set' methods:");
        System.out.println(ocky1.getName() + "'s new age: " + ocky1.getAge());
        System.out.println(ocky1.getName() + "'s new weight: " + ocky1.getWeight());
        System.out.println("Utensil's new cost: $" + spat1.getCost());
        System.out.println("Utensil's new color: " + spat1.getColor());
    }
}
