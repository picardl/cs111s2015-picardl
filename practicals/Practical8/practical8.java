//*****************************
// Layne Picard
//CMPSC 111
// Practical 8
// April 2, 2015
//
// Purpose: To practice loops and if/else statements
//*****************************

import java.util.Scanner;
import java.util.Date;

public class practical8
{
    public static void main(String [] args)
    {
        System.out.println("Layne Picard\nPractical 8\n" + new Date() + "\n");
        Scanner s = new Scanner(System.in);

        int num = 5;
        int tries = 1;
        System.out.print("Try to guess my number in between 1 and 100: ");

        int user = s.nextInt();

        while(user != num)
        {
            if (user < num)
            {
                System.out.print("Your number is too low! Enter another: ");
            }
            else if (user > num)
            {
                System.out.print("Your number is too high! Enter another: ");
            }
            user = s.nextInt();
            tries++;

        }
        System.out.println("You guessed it\nIt took you "+tries+" tries");
    }
}
