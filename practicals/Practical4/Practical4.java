//=================================================
// Honor Code: The work I am submitting is
// a result of my own thinking and efforts.

// Layne Picard
// CMPSC 111 Spring 2015
// Practical 4
// Date: February 19 2015
//
// Purpose: Explore the new gvim layout and note
// differences between this and the default
//=================================================
public class Practical4
{	public static void main(String[] args)
	{	String s1 = "Computer Science";
		int x = 111;
		String s2 = s1 + " " + x;
		String s3 = " is fun";
		String s4 = s2 + s3;
		System.out.println("s1: " + s1);
		System.out.println("s2: " + s2);
		System.out.println("s3: " + s3);
		System.out.println("s4: " + s4);
	}
}
