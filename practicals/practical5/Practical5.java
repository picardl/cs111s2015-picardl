/************************************
 Honor Code: The work I am submitting is
 a result of my own thinking and efforts.
 Layne Picard
 CMPSC 111
 26 February 2015
 Practical 5

 Basic Input with a dialog box
************************************/

import javax.swing.JOptionPane;

public class Practical5
{
    public static void main ( String[] args )
    {
		// display a dialog with a message, "welcome!" is the title of the dialog box
        JOptionPane.showMessageDialog( null, "Welcome to Java. Let's talk!", "Welcome!", 1 );
		// prompt user to enter name
		String name = JOptionPane.showInputDialog(null,"Please enter your name", "WHO ARE YOU?", 3);

		//create a message
		String message = String.format ("My cousin's name is, %s, too! ", name);

		//display the message to welcome the user by name
		JOptionPane.showMessageDialog(null, message, "cool", 1);
        //ask user where they're from
        String hometown = JOptionPane.showInputDialog(null, "Where are you from?", "Hometown", 3);
        String message2 = String.format("My cousin is from %s too...", hometown);
        //displaying message
        JOptionPane.showMessageDialog(null, message2, "Interesting", 1);
        //displaying message
        JOptionPane.showMessageDialog(null, "You're my cousin!!!", "Woah...", 0);
        //asking the user to write a letter to their uncle
        String letter = JOptionPane.showInputDialog(null, "Write a message to send to Uncle Bob", "Say Hello", 3);
// confirming their letter to uncle bob
        String message3 = String.format("Okay! I will send '%s' to Uncle Bob", letter);
        JOptionPane.showMessageDialog(null, message3, "Awesome", 2);

    } //end main
}  //end class Practical5
